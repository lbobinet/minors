use crate::data::{Field, Piece, PieceType, Line, RotationType};
use crate::input::InputHandler;
use crate::ruleset::RuleSet;
use std::cmp;
use std::collections::VecDeque;

pub struct Game<'a> {
    pub state: GameState,
    pub step: Step,
    gravity_frame_counter: FrameCounter,
    gravity_fall_size: u32,
    lock_frame_counter: FrameCounter,
    are_frame_counter: FrameCounter,
    rule_set: &'a RuleSet,
    randomizer: PieceRandomizer,
}

pub struct GameState {
    pub field: Field,
    pub score_data: ScoreData,

    pub piece: Piece,
    pub next_piece: PieceType,
}

pub struct ScoreData {
    pub level: u32,
}

pub enum Step {
    Are,
    Fall,
    ClearLines,
}

impl ScoreData {
    fn new() -> ScoreData {
        ScoreData { level: 0 }
    }
}

struct FrameCounter {
    count: u32,
    limit: u32,
}

impl FrameCounter {
    fn new() -> FrameCounter {
        FrameCounter {
            count: 0,
            limit: 0,
        }
    }

    fn init(&mut self, limit: u32) {
        self.limit = limit;
        self.reset();
    }

    fn inc(&mut self) -> bool {
        self.count = self.count + 1;
        self.count >= self.limit
    }

    fn reset(&mut self) {
        self.count = 0;
    }
}

impl<'a> Game<'a> {
    pub fn new(rule_set: &'a RuleSet) -> Game<'a> {
        let f = Field { lines: Vec::<Line>::new() };
        let game_state = GameState {
            field: f,
            piece: Piece::new(Piece::random()),
            next_piece: Piece::random(),
            score_data: ScoreData::new(),
        };
        Game {
            state: game_state,
            step: Step::Are,
            gravity_frame_counter: FrameCounter::new(),
            gravity_fall_size: 0,
            lock_frame_counter: FrameCounter::new(),
            are_frame_counter: FrameCounter::new(),
            rule_set: rule_set,
            randomizer: PieceRandomizer { history: VecDeque::new() },
        }
    }

    pub fn init(&mut self) {
        self.enter_are();
    }

    pub fn step(&mut self, input: &InputHandler) {
        match self.step {
            Step::Are => self.handle_are(input),
            Step::Fall => self.handle_fall(input),
            Step::ClearLines => self.handle_clear_lines(),
        }
    }

    fn enter_are(&mut self) {
        self.step = Step::Are;
        self.are_frame_counter.init(self.rule_set.get_are_time(&self.state.score_data));
    }

    fn enter_fall(&mut self) {
        self.step = Step::Fall;
        let gravity = self.rule_set.get_gravity(&self.state.score_data);
        let gravity_frame_number = (256 / gravity) as u32;
        self.gravity_frame_counter.init(gravity_frame_number);
        self.gravity_fall_size = if gravity <= 256 {
            1
        } else {
            (gravity / 256) as u32
        };
        self.lock_frame_counter.init(self.rule_set.get_lock_time(&self.state.score_data));
    }

    fn enter_clear_lines(&mut self) {
        self.step = Step::ClearLines;
    }

    fn handle_piece_move(&mut self, input: &InputHandler) {
        let horizontal_move = input.get_horizontal_move();
        let vertical_move = input.get_vertical_move();
        if horizontal_move != 0 {
            self.state.move_piece_if_possible(horizontal_move, 0);
        }
        if vertical_move == -1 {
            let fall = self.state.move_piece_if_possible(0, -1);
            self.gravity_frame_counter.reset();
            if !fall {
                self.state.field.lock_piece(&self.state.piece);
                self.enter_clear_lines();
            }
        } else if vertical_move == 1 {
            let max_fall = self.state.get_max_fall();
            self.state.move_piece_if_possible(0, -max_fall);
            self.gravity_frame_counter.reset();
        }
    }

    fn handle_rotation(&mut self, input: &InputHandler) {
        let rotation = input.get_rotation();
        if let Some(rt) = rotation {
            self.state.rotate_piece_if_possible(rt);
        }
    }

    fn handle_fall(&mut self, input: &InputHandler) {
        self.handle_piece_move(input);
        self.handle_rotation(input);
        let max_fall = self.state.get_max_fall();
        if max_fall == 0 {
            if self.lock_frame_counter.inc() {
                self.state.field.lock_piece(&self.state.piece);
                self.enter_clear_lines();
            }
        } else if self.gravity_frame_counter.inc() {
            self.state
                .move_piece_if_possible(0, -cmp::min(self.gravity_fall_size as i32, max_fall));
            self.gravity_frame_counter.reset();
            self.lock_frame_counter.reset();
        }
    }

    fn handle_are(&mut self, input: &InputHandler) {
        if self.are_frame_counter.inc() {
            self.state.piece = Piece::new(self.state.next_piece);
            self.state.piece.col = 3;
            self.state.piece.row = 18;
            self.state.next_piece = self.randomizer.random();

            let rotation = input.get_currently_pressed_rotation();
            if let Some(rt) = rotation {
                self.state.rotate_piece_if_possible(rt);
            }

            self.enter_fall();
        }
    }

    fn handle_clear_lines(&mut self) {
        let nb_lines = self.state.field.remove_full_lines();

        self.state.score_data.level = self.state.score_data.level + 1 + nb_lines;
        self.enter_are();
    }
}

impl GameState {
    fn move_piece_if_possible(&mut self, offset_x: i32, offset_y: i32) -> bool {
        if self.is_move_piece_possible(offset_x, offset_y) {
            self.piece.col = self.piece.col + offset_x;
            self.piece.row = self.piece.row + offset_y;
            true
        } else {
            false
        }
    }

    fn is_move_piece_possible(&self, offset_x: i32, offset_y: i32) -> bool {
        self.field.is_piece_position_free(self.piece.piece_type,
                                          self.piece.col + offset_x,
                                          self.piece.row + offset_y,
                                          self.piece.orientation)
    }

    fn get_max_fall(&self) -> i32 {
        let mut result = 1;
        while self.is_move_piece_possible(0, -result) {
            result = result + 1;
        }
        result - 1
    }

    fn rotate_piece_if_possible(&mut self, rot: RotationType) -> bool {
        let increment = match rot {
            RotationType::CW => 1,
            RotationType::CCW => 3,
        };
        let new_orientation = (self.piece.orientation + increment) % 4;

        if self.field.is_piece_position_free(self.piece.piece_type,
                                             self.piece.col,
                                             self.piece.row,
                                             new_orientation) {
            self.piece.orientation = new_orientation;
            true
        } else {
            match self.piece.piece_type {
                PieceType::I => false,
                _ => {
                    if self.field.is_piece_position_free(self.piece.piece_type,
                                                         self.piece.col + 1,
                                                         self.piece.row,
                                                         new_orientation) {
                        self.piece.col = self.piece.col + 1;
                        self.piece.orientation = new_orientation;
                        true
                    } else if self.field.is_piece_position_free(self.piece.piece_type,
                                                                self.piece.col - 1,
                                                                self.piece.row,
                                                                new_orientation) {
                        self.piece.col = self.piece.col - 1;
                        self.piece.orientation = new_orientation;
                        true
                    } else {
                        false
                    }
                }
            }
        }
    }
}

pub struct PieceRandomizer {
    history: VecDeque<PieceType>,
}

impl PieceRandomizer {
    fn random(&mut self) -> PieceType {
        let mut pt = Piece::random();
        for _ in 0..3 {
            if self.history.iter().any(|x| *x == pt) {
                pt = Piece::random();
            }
        }
        self.history.push_back(pt);
        if self.history.len() > 4 {
            self.history.pop_front();
        }
        pt
    }
}