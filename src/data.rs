use rand::{thread_rng, Rng};

#[derive(Copy, Clone)]
pub struct Block {
    pub color: &'static (u32, u32, u32),
}

#[derive(Copy, Clone)]
pub struct Piece {
    pub block: Block,
    pub piece_type: PieceType,
    pub col: i32,
    pub row: i32,
    pub orientation: usize,
}

pub struct Field {
    pub lines: Vec<Line>,
}

pub struct Line {
    pub blocks: [Option<Block>; 10],
}

use self::PieceType::*;

#[derive(Copy, Clone, PartialEq)]
pub enum PieceType {
    I,
    J,
    L,
    O,
    S,
    Z,
    T,
}

pub enum RotationType {
    CW,
    CCW,
}

impl Line {
    pub fn empty() -> Line {
        Line { blocks: [None; 10] }
    }

    fn is_full(&self) -> bool {
        !self.blocks.iter().any(|bl| bl.is_none())
    }
}

impl Field {
    pub fn is_position_free(&self, col: i32, row: i32) -> bool {
        if col < 0 || col > 9 {
            false
        } else if row < 0 {
            false
        } else if self.lines.len() as i32 <= row {
            true
        } else {
            match self.lines[row as usize].blocks[col as usize] {
                Some(_) => false,
                None => true,
            }
        }
    }

    pub fn is_piece_position_free(&self,
                                  piece_type: PieceType,
                                  col: i32,
                                  row: i32,
                                  orientation: usize)
                                  -> bool {
        let mut free = true;
        for pos in Piece::get_absolute_positions(piece_type, orientation, col, row) {
            if !self.is_position_free(pos.0, pos.1) {
                free = false;
            }
        }
        free
    }

    pub fn remove_full_lines(&mut self) -> u32 {
        let mut count = 0;
        let range = 0usize..self.lines.len();
        for row in range.rev() {
            if self.lines[row].is_full() {
                self.lines.remove(row);
                count = count + 1;
            }
        }
        count
    }

    pub fn lock_piece(&mut self, piece: &Piece) {
        for ref pos in piece.blocks_positions() {
            for _ in self.lines.len()..(pos.1 + 1) as usize {
                self.lines.push(Line::empty());
            }
            self.lines[pos.1 as usize].blocks[pos.0 as usize] = Some(piece.block);
        }
    }
}

impl Piece {
    pub fn new(piecetype: PieceType) -> Piece {
        let block = Block { color: Piece::get_block_color(piecetype) };
        Piece {
            piece_type: piecetype,
            block: block,
            col: 0,
            row: 0,
            orientation: 0,
        }
    }

    pub fn get_relative_positions(piece_type: PieceType,
                                  orientation: usize)
                                  -> &'static [(i32, i32); 4] {
        match piece_type {
            PieceType::I => &I_PIECE_POSITIONS[orientation],
            PieceType::O => &O_PIECE_POSITIONS[orientation],
            PieceType::J => &J_PIECE_POSITIONS[orientation],
            PieceType::L => &L_PIECE_POSITIONS[orientation],
            PieceType::T => &T_PIECE_POSITIONS[orientation],
            PieceType::S => &S_PIECE_POSITIONS[orientation],
            PieceType::Z => &Z_PIECE_POSITIONS[orientation],
        }
    }

    pub fn get_absolute_positions(piece_type: PieceType,
                                  orientation: usize,
                                  col: i32,
                                  row: i32)
                                  -> Vec<(i32, i32)> {
        Piece::get_relative_positions(piece_type, orientation)
            .iter()
            .map(|x| (x.0 + col, x.1 + row))
            .collect()
    }

    pub fn blocks_positions(&self) -> Vec<(i32, i32)> {
        Piece::get_absolute_positions(self.piece_type, self.orientation, self.col, self.row)
    }

    pub fn get_block_color(piece_type: PieceType) -> &'static (u32, u32, u32) {
        match piece_type {
            PieceType::I => &I_PIECE_COLOR,
            PieceType::J => &J_PIECE_COLOR,
            PieceType::L => &L_PIECE_COLOR,
            PieceType::S => &S_PIECE_COLOR,
            PieceType::Z => &Z_PIECE_COLOR,
            PieceType::T => &T_PIECE_COLOR,
            PieceType::O => &O_PIECE_COLOR,
        }
    }

    pub fn random() -> PieceType {
        let mut rng = thread_rng();
        let index: u8 = rng.gen_range(0, 7);
        PIECE_TYPES[index as usize]
    }
}

static PIECE_TYPES: [PieceType; 7] = [I, J, L, O, S, Z, T];

static I_PIECE_POSITIONS: [[(i32, i32); 4]; 4] = [[(0, 2), (1, 2), (2, 2), (3, 2)],
                                                  [(2, 0), (2, 1), (2, 2), (2, 3)],
                                                  [(0, 2), (1, 2), (2, 2), (3, 2)],
                                                  [(2, 0), (2, 1), (2, 2), (2, 3)]];

static O_PIECE_POSITIONS: [[(i32, i32); 4]; 4] = [[(1, 0), (2, 0), (1, 1), (2, 1)],
                                                  [(1, 0), (2, 0), (1, 1), (2, 1)],
                                                  [(1, 0), (2, 0), (1, 1), (2, 1)],
                                                  [(1, 0), (2, 0), (1, 1), (2, 1)]];

static T_PIECE_POSITIONS: [[(i32, i32); 4]; 4] = [[(0, 1), (1, 1), (2, 1), (1, 0)],
                                                  [(1, 0), (1, 1), (1, 2), (0, 1)],
                                                  [(0, 0), (1, 0), (2, 0), (1, 1)],
                                                  [(1, 0), (1, 1), (1, 2), (2, 1)]];

static Z_PIECE_POSITIONS: [[(i32, i32); 4]; 4] = [[(0, 1), (1, 1), (1, 0), (2, 0)],
                                                  [(1, 0), (1, 1), (2, 1), (2, 2)],
                                                  [(0, 1), (1, 1), (1, 0), (2, 0)],
                                                  [(1, 0), (1, 1), (2, 1), (2, 2)]];

static S_PIECE_POSITIONS: [[(i32, i32); 4]; 4] = [[(0, 0), (1, 0), (1, 1), (2, 1)],
                                                  [(1, 0), (1, 1), (0, 1), (0, 2)],
                                                  [(0, 0), (1, 0), (1, 1), (2, 1)],
                                                  [(1, 0), (1, 1), (0, 1), (0, 2)]];

static J_PIECE_POSITIONS: [[(i32, i32); 4]; 4] = [[(0, 1), (1, 1), (2, 1), (2, 0)],
                                                  [(1, 0), (1, 1), (1, 2), (0, 0)],
                                                  [(0, 0), (1, 0), (2, 0), (0, 1)],
                                                  [(1, 0), (1, 1), (1, 2), (2, 2)]];

static L_PIECE_POSITIONS: [[(i32, i32); 4]; 4] = [[(0, 1), (1, 1), (2, 1), (0, 0)],
                                                  [(1, 0), (1, 1), (1, 2), (0, 2)],
                                                  [(0, 0), (1, 0), (2, 0), (2, 1)],
                                                  [(1, 0), (1, 1), (1, 2), (2, 0)]];

static I_PIECE_COLOR: (u32, u32, u32) = (203, 19, 10);
static J_PIECE_COLOR: (u32, u32, u32) = (0, 32, 192);
static L_PIECE_COLOR: (u32, u32, u32) = (220, 100, 0);
static S_PIECE_COLOR: (u32, u32, u32) = (141, 5, 141);
static Z_PIECE_COLOR: (u32, u32, u32) = (5, 148, 0);
static T_PIECE_COLOR: (u32, u32, u32) = (0, 131, 175);
static O_PIECE_COLOR: (u32, u32, u32) = (149, 122, 0);