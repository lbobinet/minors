#[macro_use]
extern crate glium;

use self::ruleset::DeathRuleSet;
use self::game::Game;
use glutin::{GlRequest, GlProfile, Robustness, Api, Event, VirtualKeyCode, ElementState};
use self::input::{InputHandler, InputHandlerGlutin};

mod data;
mod render;
mod game;
mod input;
mod ruleset;


fn main() {
    use glium::{DisplayBuild, Surface};

    let window = glutin::WindowBuilder::new()
        .with_vsync()
        .with_gl(GlRequest::Specific(Api::OpenGl, (3, 0)))
        .with_gl_profile(GlProfile::Compatibility)
        .with_gl_robustness(Robustness::TryRobustNoResetNotification)
        .with_depth_buffer(24)
        .build_glium()
        .unwrap();
    let renderer = render::Renderer::new(&window);
    let mut input_handler = InputHandlerGlutin::new();

    let rule_set = DeathRuleSet;
    let mut game = Game::new(&rule_set);
    game.init();

    let mut running = true;
    while running {
        input_handler.init_frame();
        for event in window.poll_events() {
            match event {
                Event::Closed => running = false,
                Event::KeyboardInput(event_type, _scan_code, virtual_key_code) => {
                    println!("{:?}", event);

                    if virtual_key_code.is_some() {
                        let virtual_key_code = virtual_key_code.unwrap();

                        match event_type {
                            ElementState::Pressed => input_handler.pressed(virtual_key_code),
                            ElementState::Released => input_handler.released(virtual_key_code),
                        }

                        match virtual_key_code {
                            VirtualKeyCode::Escape => running = false,
                            _ => (),
                        }
                    }
                }
                _ => (),
            }
        }
        input_handler.compute();
        game.step(&mut input_handler);

        let mut target = window.draw();
        target.clear_color_and_depth((0.0, 0.0, 0.0, 0.0), 1.0);
        game.render(&mut target, &renderer);
        target.finish().unwrap();
    }
}
