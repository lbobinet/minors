use crate::data::RotationType;
use std::cmp;
use glutin::VirtualKeyCode;

pub trait InputHandler {
    fn init_frame(&mut self);
    fn compute(&mut self);
    fn get_horizontal_move(&self) -> i32;
    fn get_vertical_move(&self) -> i32;
    fn get_rotation(&self) -> Option<RotationType>;
    fn get_currently_pressed_rotation(&self) -> Option<RotationType>;
}

pub struct InputHandlerGlutin {
    das_counter: i32,
    left_pressed: bool,
    right_pressed: bool,
    cw_pressed: bool,
    ccw_pressed: bool,
    cw_was_pressed: bool,
    ccw_was_pressed: bool,
    up_pressed: bool,
    down_pressed: bool,
}

impl InputHandlerGlutin {
    pub fn new() -> InputHandlerGlutin {
        InputHandlerGlutin {
            das_counter: 0,
            left_pressed: false,
            right_pressed: false,
            up_pressed: false,
            down_pressed: false,
            cw_pressed: false,
            ccw_pressed: false,
            cw_was_pressed: false,
            ccw_was_pressed: false,
        }
    }

    pub fn pressed(&mut self, code: VirtualKeyCode) {
        match code {
            VirtualKeyCode::Left => self.left_pressed = true,
            VirtualKeyCode::Right => self.right_pressed = true,
            VirtualKeyCode::Up => self.up_pressed = true,
            VirtualKeyCode::Down => self.down_pressed = true,
            VirtualKeyCode::W => self.ccw_pressed = true,
            VirtualKeyCode::X => self.cw_pressed = true,
            _ => (),
        }
    }

    pub fn released(&mut self, code: VirtualKeyCode) {
        match code {
            VirtualKeyCode::Left => self.left_pressed = false,
            VirtualKeyCode::Right => self.right_pressed = false,
            VirtualKeyCode::Up => self.up_pressed = false,
            VirtualKeyCode::Down => self.down_pressed = false,
            VirtualKeyCode::W => self.ccw_pressed = false,
            VirtualKeyCode::X => self.cw_pressed = false,
            _ => (),
        }
    }

    fn update_das(&mut self) {
        if self.left_pressed {
            self.das_counter = cmp::min(self.das_counter, 0) - 1;
        } else if self.right_pressed {
            self.das_counter = cmp::max(self.das_counter, 0) + 1;
        } else {
            self.das_counter = 0;
        }
    }
}

impl InputHandler for InputHandlerGlutin {
    fn get_horizontal_move(&self) -> i32 {
        if self.das_counter == -1 || self.das_counter <= -10 {
            -1
        } else if self.das_counter == 1 || self.das_counter >= 10 {
            1
        } else {
            0
        }
    }

    fn get_vertical_move(&self) -> i32 {
        if self.down_pressed {
            -1
        } else if self.up_pressed {
            1
        } else {
            0
        }
    }

    fn get_rotation(&self) -> Option<RotationType> {
        if self.ccw_pressed && !self.ccw_was_pressed {
            Some(RotationType::CCW)
        } else if self.cw_pressed && !self.cw_was_pressed {
            Some(RotationType::CW)
        } else {
            None
        }
    }

    fn get_currently_pressed_rotation(&self) -> Option<RotationType> {
        if self.ccw_pressed {
            Some(RotationType::CCW)
        } else if self.cw_pressed {
            Some(RotationType::CW)
        } else {
            None
        }
    }

    fn compute(&mut self) {
        self.update_das();
    }

    fn init_frame(&mut self) {
        self.cw_was_pressed = self.cw_pressed;
        self.ccw_was_pressed = self.ccw_pressed;
    }
}
