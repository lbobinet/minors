use glium;
use genmesh;
use obj;

use glium::vertex::VertexBufferAny;
use nalgebra::*;
use num::traits::{One, Zero};
use crate::data::{Block, Line, Field, Piece, PieceType};

use glium::Surface;
use glium::backend::glutin_backend::GlutinFacade;
use crate::game::{Game, Step};

pub struct Renderer {
    block_buffer: VertexBufferAny,
    border_buffer: VertexBufferAny,
    block_program: glium::Program,
    indices: glium::index::NoIndices,
    view_matrix: [[f32; 4]; 4],
    projection_matrix: [[f32; 4]; 4],
}

impl Block {
    fn render(&self,
              target: &mut glium::Frame,
              renderer: &Renderer,
              col_offset: i32,
              row_offset: i32,
              is_locked: bool)
              -> () {
        renderer.render_block(target, col_offset, row_offset, self.color, is_locked).unwrap();
    }
}

impl Line {
    fn render(&self, target: &mut glium::Frame, renderer: &Renderer, row_offset: i32) -> () {
        let mut line_col = 0;
        for &block in self.blocks.iter() {
            if let Some(b) = block {
                b.render(target, renderer, line_col, row_offset, true);
            }
            line_col = line_col + 1;
        }
    }
}

impl Field {
    pub fn render(&self, target: &mut glium::Frame, renderer: &Renderer) -> () {
        let mut line_row = 0;
        for ref line in self.lines.iter() {
            line.render(target, renderer, line_row);
            line_row = line_row + 1;
        }
    }
}

impl Piece {
    pub fn render(&self, target: &mut glium::Frame, renderer: &Renderer) -> () {
        for position in self.blocks_positions() {
            let (col, row) = position;
            self.block.render(target, renderer, col, row, false);
        }
    }

    pub fn render_next_piece(piece_type: PieceType,
                             target: &mut glium::Frame,
                             renderer: &Renderer)
                             -> () {
        for position in Piece::get_relative_positions(piece_type, 0) {
            let (col, row) = *position;
            let color = Piece::get_block_color(piece_type);
            renderer.render_block(target, col + 3, row + 24, color, false).unwrap();
        }
    }
}

impl<'a> Game<'a> {
    pub fn render(&self, target: &mut glium::Frame, renderer: &Renderer) -> () {
        renderer.render_border(target);
        self.state.field.render(target, renderer);
        Piece::render_next_piece(self.state.next_piece, target, renderer);

        if let Step::Fall = self.step {
            self.state.piece.render(target, renderer);
        }
        renderer.render_text(target);
    }
}

impl Renderer {
    pub fn new(display: &GlutinFacade) -> Renderer {
        let bb = load_wavefront(&display, include_bytes!("../resources/tetriblock.obj"));
        let bb2 = load_wavefront(&display, include_bytes!("../resources/border.obj"));

        let bp = glium::Program::from_source(display,
                                             include_str!("../resources/vs-block.txt"),
                                             include_str!("../resources/fs-block.txt"),
                                             None)
            .unwrap();
        let ind = glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList);
        // let mut view = Iso3::<f32>::one();
        let view = Iso3::<f32>::look_at_z(&Pnt3::<f32>::new(5.0, -10.0, 30.0),
                                          &Pnt3::<f32>::new(5.0, -10.0, 0.0),
                                          &Vec3::<f32>::new(0.0, 1.0, 0.0));
        let proj = *PerspMat3::<f32>::new(1.0, 1.0, 1.0, 1000.0).as_mat().as_ref();

        Renderer {
            block_buffer: bb,
            border_buffer: bb2,
            block_program: bp,
            indices: ind,
            view_matrix: *view.to_homogeneous().as_ref(),
            projection_matrix: proj,
        }
    }

    fn render_block(&self,
                    target: &mut glium::Frame,
                    x: i32,
                    y: i32,
                    color: &(u32, u32, u32),
                    is_locked: bool)
                    -> Result<(), ()> {
        let model_matrix = Iso3::<f32>::new(Vec3::<f32>::new(x as f32, y as f32, 0.0),
                                            Vec3::<f32>::zero());
        // println!("{:?}", model_matrix.to_homogeneous().as_array());
        let r = color.0 as f32 / 255.0;
        let g = color.1 as f32 / 255.0;
        let b = color.2 as f32 / 255.0;
        // println!("{} {} {}", r, g, b);
        let uniforms = uniform! {
            P: self.projection_matrix,
            M: *model_matrix.to_homogeneous().as_ref(),
            V: self.view_matrix,
            LightPower: if is_locked {1200f32} else {2000f32},
            LightColor: *Vec3::<f32>::one().as_ref(),
            LightPosition_worldspace: *Vec3::<f32>::new(10.0, 20.0, 30.0).as_ref(),
            MaterialDiffuseColor: *Vec3::<f32>::new(r, g, b).as_ref()
        };

        // drawing a frame
        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::DepthTest::IfLess,
                write: true,
                ..Default::default()
            },
            ..Default::default()
        };

        target.draw(&self.block_buffer,
                  &self.indices,
                  &self.block_program,
                  &uniforms,
                  &params)
            .unwrap();
        Result::Ok(())
    }

    fn render_border(&self, target: &mut glium::Frame) {
        let model_matrix = Iso3::<f32>::new(Vec3::<f32>::new(-0.5, -0.5, 0.0), Vec3::<f32>::zero());

        let uniforms = uniform! {
            P: self.projection_matrix,
            M: *model_matrix.to_homogeneous().as_ref(),
            V: self.view_matrix,
            LightPower: 2000f32,
            LightColor: *Vec3::<f32>::one().as_ref(),
            LightPosition_worldspace: *Vec3::<f32>::new(10.0, 20.0, 30.0).as_ref(),
            MaterialDiffuseColor: *Vec3::<f32>::new(1.0, 1.0, 1.0).as_ref()
        };

        // drawing a frame
        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::DepthTest::IfLess,
                write: true,
                ..Default::default()
            },
            ..Default::default()
        };

        target.draw(&self.border_buffer,
                  &self.indices,
                  &self.block_program,
                  &uniforms,
                  &params)
            .unwrap();
    }

    fn render_text(&self, _: &mut glium::Frame) {}
}

pub fn load_wavefront(display: &GlutinFacade, data: &[u8]) -> VertexBufferAny {
    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 3],
        normal: [f32; 3],
        texture: [f32; 2],
    }

    implement_vertex!(Vertex, position, normal, texture);

    let mut data = ::std::io::BufReader::new(data);
    let data = obj::Obj::load(&mut data);

    let mut vertex_data = Vec::new();

    for object in data.object_iter() {
        for shape in object.group_iter().flat_map(|g| g.indices().iter()) {
            match shape {
                &genmesh::Polygon::PolyTri(genmesh::Triangle { x: v1, y: v2, z: v3 }) => {
                    for v in [v1, v2, v3].iter() {
                        let position = data.position()[v.0];
                        let texture = v.1.map(|index| data.texture()[index]);
                        let normal = v.2.map(|index| data.normal()[index]);

                        let texture = texture.unwrap_or([0.0, 0.0]);
                        let normal = normal.unwrap_or([0.0, 0.0, 0.0]);

                        vertex_data.push(Vertex {
                            position: position,
                            normal: normal,
                            texture: texture,
                        })
                    }
                }
                _ => unimplemented!(),
            }
        }
    }

    glium::vertex::VertexBuffer::new(display, &vertex_data).unwrap().into_vertex_buffer_any()
}
