use crate::game::ScoreData;
use crate::ruleset::RuleSet;

#[allow(dead_code)]
pub struct MasterRuleSet;

impl RuleSet for MasterRuleSet {
    fn get_are_time(&self, score_data: &ScoreData) -> u32 {
        if score_data.level <= 700 {
            25
        } else if score_data.level <= 800 {
            16
        } else {
            12
        }
    }

    fn get_lock_time(&self, score_data: &ScoreData) -> u32 {
        if score_data.level <= 900 {
            30
        } else {
            17
        }
    }

    fn get_gravity(&self, score_data: &ScoreData) -> u32 {
        if score_data.level <= 30 {4}
        else if score_data.level <= 35 {6}
        else if score_data.level <= 40 {8}
        else if score_data.level <= 50 {10}
        else if score_data.level <= 60 {12}
        else if score_data.level <= 70 {16}
        else if score_data.level <= 80 {32}
        else if score_data.level <= 90 {48}
        else if score_data.level <= 100 {64}
        else if score_data.level <= 120 {80}
        else if score_data.level <= 140 {96}
        else if score_data.level <= 160 {112}
        else if score_data.level <= 170 {128}
        else if score_data.level <= 200 {144}
        else if score_data.level <= 220 {4}
        else if score_data.level <= 230 {32}
        else if score_data.level <= 233 {64}
        else if score_data.level <= 236 {96}
        else if score_data.level <= 239 {128}
        else if score_data.level <= 243 {160}
        else if score_data.level <= 247 {192}
        else if score_data.level <= 251 {224}
        else if score_data.level <= 300 {256}
        else if score_data.level <= 330 {512}
        else if score_data.level <= 360 {768}
        else if score_data.level <= 400 {1024}
        else if score_data.level <= 420 {1280}
        else if score_data.level <= 450 {1024}
        else if score_data.level <= 500 {768}
        else {5120}
    }
}