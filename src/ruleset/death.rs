use crate::game::ScoreData;
use crate::ruleset::RuleSet;

pub struct DeathRuleSet;

impl RuleSet for DeathRuleSet {
    fn get_are_time(&self, score_data: &ScoreData) -> u32 {
        if score_data.level <= 100 {
            16
        } else if score_data.level <= 300 {
            12
        } else if score_data.level <= 400 {
            6
        } else if score_data.level <= 500 {
            5
        } else {
            4
        }
    }

    fn get_lock_time(&self, score_data: &ScoreData) -> u32 {
        if score_data.level <= 100 {
            30
        } else if score_data.level <= 200 {
            26
        } else if score_data.level <= 300 {
            22
        } else if score_data.level <= 400 {
            18
        } else {
            15
        }
    }

    fn get_gravity(&self, _: &ScoreData) -> u32 {
        5120
    }
}