mod death;
mod master;

pub use self::death::DeathRuleSet;
pub use self::master::MasterRuleSet;
use crate::game::ScoreData;

/// Test doc
pub trait RuleSet {
    fn get_are_time(&self, score_data: &ScoreData) -> u32;
    fn get_lock_time(&self, score_data: &ScoreData) -> u32;
    fn get_gravity(&self, score_data: &ScoreData) -> u32;
}